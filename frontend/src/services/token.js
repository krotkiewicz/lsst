import store from 'store';


class Token {
  constructor() {
    this.token = store.get('token', null);
  }
  set(token) {
    this.token = token;
    store.set('token', token);
  }
  get() {
    return this.token;
  }
  del() {
    store.remove('token');
    this.token = null;
  }
}

export default new Token();
