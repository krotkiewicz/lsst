import _ from 'lodash';

export default {
  activity_types: [
    {
      type: 'hr_diagram',
      text: 'The H-R diagram - a window to the stars',
    },
  ],
  get_activity_type(type) {
    return _.find(this.activity_types, a => a.type === type);
  },
};
