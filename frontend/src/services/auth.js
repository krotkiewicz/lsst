import { jc } from './http';
import token from './token';

class Auth {
  constructor() {
    this.logged = false;
    this.user = null;
  }

  fetchData() {
    const self = this;
    const tokenStr = token.get();
    if (tokenStr) {
      jc.defaults.headers.common.Authorization = `JWT ${tokenStr}`;
      return jc.get('/auth/user/')
        .then((resp) => {
          self.user = resp.data;
          self.logged = true;
        })
        .catch((err) => {
          self.logoutUser();
          throw err;
        });
    }
    return Promise.reject();
  }

  loginUser(email, password) {
    const self = this;
    self.logoutUser();
    return jc.post('/auth/login/', { email, password }).then(resp => self.submitToken(resp.data.token));
  }

  logoutUser() {
    const self = this;
    self.user = null;
    self.logged = false;
    token.del();
    delete jc.defaults.headers.common.Authorization;
  }

  submitToken(newToken) {
    token.set(newToken);
    return this.fetchData();
  }

}

export default new Auth();
