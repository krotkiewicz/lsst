import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/components/HomePage';
import ActivityPage from '@/components/ActivityPage';
import ActivitiesPage from '@/components/ActivitiesPage';
import Login from '@/components/Login';
import RootPage from '@/components/RootPage';
import { auth } from '@/services';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: RootPage,
      meta: {
        requiresAuth: true,
      },
      children: [
        {
          path: '',
          name: 'home',
          component: HomePage,
        },
        {
          path: '/activities',
          name: 'activities',
          component: ActivitiesPage,
        },
        {
          path: '/activities/:id',
          name: 'activity',
          component: ActivityPage,
        },
      ],
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        if (auth.logged) {
          next({ name: 'home' });
        } else {
          next();
        }
      },
    },
  ],
});

// eslint-ignore

router.beforeEach((to, from, next) => {
  auth.fetchData().catch(() => {}).then(() => {
    if (!auth.logged && to.matched.some(record => record.meta.requiresAuth)) {
      next({
        name: 'login',
      });
    } else {
      next();
    }
  });
});

export default router;
