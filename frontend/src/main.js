// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueMoment from 'vue-moment';
import Vuelidate from 'vuelidate';
import App from './App';
import router from './router';
import './scss/main.scss';
import store from './stores';
import choices from './services/choices';

Vue.config.productionTip = false;
Vue.use(VueMoment);
Vue.use(Vuelidate);


Vue.mixin({
  data() {
    return {
      choices,
    };
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App },
});
