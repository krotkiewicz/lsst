import moment from 'moment';
import _ from 'lodash';
import choices from '@/services/choices';

export default {
  state: {
    activities: [
      {
        id: 1,
        description: 'Activity 1',
        activity_type: 'hr_diagram',
        activity_type_text: 'The H-R diagram - a window to the stars',
        created_at: moment(),
      },
      {
        id: 2,
        description: 'Activity 2',
        activity_type: 'hr_diagram',
        activity_type_text: 'The H-R diagram - a window to the stars',
        created_at: moment(),
      },
      {
        id: 3,
        description: 'Activity 3',
        activity_type: 'hr_diagram',
        activity_type_text: 'The H-R diagram - a window to the stars',
        created_at: moment(),
      },
      {
        id: 4,
        description: 'Activity 4',
        activity_type: 'hr_diagram',
        activity_type_text: 'The H-R diagram - a window to the stars',
        created_at: moment(),
      },
      {
        id: 5,
        description: 'Activity 5',
        activity_type: 'hr_diagram',
        activity_type_text: 'The H-R diagram - a window to the stars',
        created_at: moment(),
      },
    ],
  },
  getters: {
    activities(state) {
      return state.activities;
    },
  },
  mutations: {
    addActivity(state, activity) {
      state.activities.unshift(activity);
    },
    removeActivity(state, activityId) {
      const activity = _.find(state.activities, a => a.id === activityId);
      const index = state.activities.indexOf(activity);
      state.activities.splice(index, 1);
    },
    editActivity(state, newActivity) {
      const index = _.findIndex(state.activities, a => a.id === newActivity.id);
      state.activities.splice(index, 1, newActivity);
    },
  },
  actions: {
    addActivity({ commit }, activity) {
      activity.isNew = true;
      activity.created_at = moment();
      activity.id = _.random(0, 10000000);
      activity.activity_type_text = choices.get_activity_type(activity.activity_type).text;
      commit('addActivity', activity);
    },
    editActivity({ commit, state }, activity) {
      activity.activity_type_text = choices.get_activity_type(activity.activity_type).text;
      commit('editActivity', activity);
    },
    removeActivity({ commit }, { id }) {
      commit('removeActivity', id);
    },
  },
};
