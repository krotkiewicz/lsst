import Vuex from 'vuex';
import Vue from 'vue';
import activities from './activities';
import students from './students';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,
  modules: {
    activities,
    students,
  },
});

export default store;
