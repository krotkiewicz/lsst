import moment from 'moment';

export default {
  state: {
    students: [
      {
        id: 1,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 2,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 3,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 4,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 5,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 6,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 7,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
      {
        id: 8,
        email: 'konrad.rotkiewicz@example.com',
        full_name: 'Konrad Rotkiewicz',
        created_at: moment(),
      },
    ],
  },
  getters: {
    students(state) {
      return state.students;
    },
  },
  mutations: {
  },
  actions: {
  },
};
